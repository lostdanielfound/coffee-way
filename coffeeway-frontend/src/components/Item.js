import React from 'react';

const Item = (props) => {
  return (
    <div>
      <h3>I drank {props.coffeegrams}</h3>
      <h4>On this date: {props.date}</h4>
    </div>
  )
}

export default Item;