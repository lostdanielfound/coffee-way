import List from './components/List'
import './assets/dist/css/bootstrap.min.css'

function App() {
  return (
    <div className='container'>
      <List />
    </div>
  );
}

export default App;
